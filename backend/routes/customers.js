const express = require('express')
const router = express.Router()
const cusController = require('../controller/CusController')

router.get('/', cusController.getCustomers)

router.get('/:id', cusController.getCustomer)

router.post('/', cusController.addCustomer)

router.put('/', cusController.updateCustomer)

router.delete('/:id', cusController.deleteCustomer)

module.exports = router
