const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new mongoose.Schema({
  login: String,
  password: String,
  status: Boolean
})

module.exports = mongoose.model('User', userSchema)