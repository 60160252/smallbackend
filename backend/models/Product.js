const mongoose = require('mongoose')
const Schema = mongoose.Schema
const productSchema = new mongoose.Schema({
  name: String,
  price: Number,
  cost: Number
})

module.exports = mongoose.model('Product', productSchema)