const mongoose = require('mongoose')
const Schema = mongoose.Schema
const cusSchema = new mongoose.Schema({
  name: String,
  address: String,
  dob: String,
  gender: String,
  mobile: String,
  email: String
})

module.exports = mongoose.model('Customer', cusSchema)